# Installation
> `npm install --save @types/react-lifecycles-compat`

# Summary
This package contains type definitions for react-lifecycles-compat (https://github.com/reactjs/react-lifecycles-compat#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-lifecycles-compat

Additional Details
 * Last updated: Thu, 17 Jan 2019 00:06:35 GMT
 * Dependencies: @types/react
 * Global values: none

# Credits
These definitions were written by bySabi Files <https://github.com/bySabi>.
