Source: node-react-lifecycles-compat
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Roland Mas <lolando@debian.org>
Testsuite: autopkgtest-pkg-nodejs
Build-Depends:
 debhelper-compat (= 13)
 , dh-sequence-nodejs
Standards-Version: 4.6.1
Homepage: https://github.com/reactjs/react-lifecycles-compat#readme
Vcs-Git: https://salsa.debian.org/js-team/node-react-lifecycles-compat.git
Vcs-Browser: https://salsa.debian.org/js-team/node-react-lifecycles-compat
Rules-Requires-Root: no

Package: node-react-lifecycles-compat
Architecture: all
Depends:
 ${misc:Depends}
 ,
Multi-Arch: foreign
Description: Backwards compatibility polyfill for React class components
 React version 17 will deprecate several of the class component API
 lifecycles: componentWillMount, componentWillReceiveProps, and
 componentWillUpdate.  A couple of new lifecycles are also being added
 to better support async rendering mode.
 .
 Typically, this type of change would require third party libraries to
 release a new major version in order to adhere to semver. However,
 the react-lifecycles-compat polyfill offers a way to use the new
 lifecycles with older versions of React as well (0.14.9+) so no
 breaking release is required. This enables shared libraries to
 support both older and newer versions of React simultaneously.
 .
 Node.js is an event-based server-side JavaScript engine.
